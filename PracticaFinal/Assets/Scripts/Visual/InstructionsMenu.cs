using Logic;
using UnityEngine;
using UnityEngine.UI;

namespace Visual
{
    public class InstructionsMenu : MonoBehaviour
    {
        [SerializeField] private GameObject storyPanel;
        [SerializeField] private Button storyPanelOkButton;
    
        [SerializeField] private GameObject elementsPanel;
        [SerializeField] private Button elementsPanelOkButton;

        [SerializeField] private GameObject controlsPanel;
        [SerializeField] private Button controlsPanelOkButton;

        void Start()
        {
            storyPanel.SetActive(true);
            elementsPanel.SetActive(false);
            controlsPanel.SetActive(false);
        
            storyPanelOkButton.onClick.AddListener(GoToElementsPanel);
            elementsPanelOkButton.onClick.AddListener(GoToControlsPanel);
            controlsPanelOkButton.onClick.AddListener(StartGame);
        }

        private void GoToElementsPanel()
        {
            storyPanel.SetActive(false);
            elementsPanel.SetActive(true);
        }

        private void GoToControlsPanel(){
            elementsPanel.SetActive(false);
            controlsPanel.SetActive(true);
        }

        /// <summary>
        /// We will save this to 
        /// </summary>
        private void StartGame()
        {
            PlayerPrefs.SetInt("Instructions", 1);
            PlayerPrefs.Save();
            GameManager.Instance.StartGame();
        }

    }
}
