using UnityEngine;
using UnityEngine.UI;

namespace Visual
{
    /// <summary>
    /// Class to manage the different keys of the player HUD
    /// </summary>
    public class KeysSpriteManager : MonoBehaviour
    {
        [SerializeField] private GameObject keysPanel;
        [SerializeField] private Image keysImage;
        [SerializeField] private Sprite chestKeysSprite;
        [SerializeField] private Sprite doorKeysSprite;

        public void HideKeysSprite()
        {
            keysPanel.SetActive(false);
        }
        
        public void ShowChestKeysSprite()
        {
            keysPanel.SetActive(true);
            keysImage.sprite = chestKeysSprite;
        }

        public void ShowDoorKeysSprite()
        {
            keysPanel.SetActive(true);
            keysImage.sprite = doorKeysSprite;
        }
    }
}
