using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Visual
{
    public class CreditsMenu : MonoBehaviour
    {
        [SerializeField] private Button backButton;

        private void Start()
        {
            backButton.onClick.AddListener(OnBackButtonClicked);
        }

        private void OnBackButtonClicked()
        {
            SceneManager.LoadScene("MainMenu");
        }
    
    }
}
