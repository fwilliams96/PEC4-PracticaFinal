using UnityEngine;
using UnityEngine.UI;

namespace Visual
{
    public class ToggleLight : MonoBehaviour
    {

        [SerializeField] private Image lightImage;
        [SerializeField] private Sprite switchedOnSprite;
        [SerializeField] private Sprite switchedOffSprite;
        [SerializeField] private bool switchedOn = false;
    
        void Start()
        {
            ChangeSprite();
        }

        public void TurnOn()
        {
            switchedOn = true;
            ChangeSprite();
        }

        public void TurnOff()
        {
            switchedOn = false;
            ChangeSprite();
        }

        public void Toggle()
        {
            switchedOn = !switchedOn;
            ChangeSprite();
        }

        private void ChangeSprite()
        {
            if (switchedOn)
            {
                lightImage.sprite = switchedOnSprite;
            }
            else
            {
                lightImage.sprite = switchedOffSprite;
            }
        }
    }
}
