using System;
using UnityEngine;
using UnityEngine.UI;

namespace Visual
{
    public class PlayerHUD : MonoBehaviour
    {
        [SerializeField] private Text woodCountText;
        [SerializeField] private KeysSpriteManager keysSpriteManager;
        [SerializeField] private GameObject potionPanel;

        private void Start()
        {
            keysSpriteManager.HideKeysSprite();
            potionPanel.SetActive(false);
        }

        public void UpdateWoodCount(int woodCount)
        {
            woodCountText.text = woodCount.ToString();
        }
    
        public void HideKeys()
        {
            keysSpriteManager.HideKeysSprite();
        }

        public void ShowChestKeys()
        {
            keysSpriteManager.ShowChestKeysSprite();
        }
        
        public void ShowDoorKeys()
        {
            keysSpriteManager.ShowDoorKeysSprite();
        }

        public void ShowPotion()
        {
            potionPanel.SetActive(true);
        }

        public void HidePotion()
        {
            potionPanel.SetActive(false);
        }
    }
}
