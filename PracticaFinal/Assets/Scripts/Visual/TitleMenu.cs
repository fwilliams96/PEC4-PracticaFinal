using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Visual
{
    public class TitleMenu : MonoBehaviour
    {
        [SerializeField] private Button playButton;
        [SerializeField] private Button exitButton;

        private void Start()
        {
            playButton.onClick.AddListener(OnPlayButtonClicked);
            exitButton.onClick.AddListener(OnExitButtonClicked);
        }

        private void OnPlayButtonClicked()
        {
            SceneManager.LoadScene("MainMenu");
        }

    
        private void OnExitButtonClicked()
        {
            Application.Quit();
        }
    }
}
