using System;
using System.Collections;
using UnityEngine;

namespace Visual
{
    public class GameCanvasManager : MonoBehaviour
    {
        [SerializeField] private InstructionsMenu instructionsMenu;
        [SerializeField] private PlayerHUD playerHud;
        [SerializeField] private GameObject pausePanel;
        [SerializeField] private MessageManager warningMessageManager;
        [SerializeField] private MessageManager infoMessageManager;
        [SerializeField] private ToggleLight lightBulbLight;
        [SerializeField] private ToggleLight flashLightLight;
        [SerializeField] private GameObject bluePotionPanel;
        [SerializeField] private GameObject gameOverPanel;
        [SerializeField] private GameObject winPanel;
        
        public bool PauseMenuEnabled { get; private set; } = false;

        /// <summary>
        /// We will show the instructions panel first and then the rest will be enabled via code
        /// </summary>
        private void Start()
        {
            instructionsMenu.gameObject.SetActive(true);
            pausePanel.SetActive(false);
            gameOverPanel.SetActive(false);
            bluePotionPanel.SetActive(false);
            winPanel.SetActive(false);
        }

        /// <summary>
        /// When the player finishes reading the instructions
        /// </summary>
        public void CloseInstructionsMenu()
        {
            instructionsMenu.gameObject.SetActive(false);
        }


        /// <summary>
        /// Here we update the number of wood on the HUD
        /// </summary>
        /// <param name="woodCount"></param>
        public void UpdateWoodCount(int woodCount)
        {
            playerHud.UpdateWoodCount(woodCount);
        }

        /// <summary>
        /// Here we hide the keys image of the HUD
        /// </summary>
        public void HideKeys()
        {
            playerHud.HideKeys();
        }

        /// <summary>
        /// Show the chest keys of the HUD
        /// </summary>
        public void ShowChestKeys()
        {
            playerHud.ShowChestKeys();
        }
        
        /// <summary>
        /// Show the door keys of the HUD (different than the chest keys)
        /// </summary>
        public void ShowDoorKeys()
        {
            playerHud.ShowDoorKeys();
        }

        /// <summary>
        /// Show the potion image on the player HUD
        /// </summary>
        public void ShowPotion()
        {
            playerHud.ShowPotion();
        }
        
        /// <summary>
        /// Here the potion image is hidden from the player HUD
        /// </summary>
        public void HidePotion()
        {
            playerHud.HidePotion();
        }

        /// <summary>
        /// Toggle the pause menu
        /// </summary>
        public void TogglePauseMenu()
        {
            PauseMenuEnabled = !PauseMenuEnabled;
            pausePanel.SetActive(PauseMenuEnabled);
        }

        /// <summary>
        /// Here we will show a message below the HUD as long as indicated in the time parameter
        /// </summary>
        /// <param name="message"></param>
        /// <param name="time"></param>
        public void ShowWarningMessage(string message, float time)
        {
            warningMessageManager.ShowMessage(message, time);
        }

        /// <summary>
        /// Here the bottom message will be displayed but it won't have a limit time
        /// </summary>
        /// <param name="message"></param>
        public void ShowInfoMessage(string message)
        {
            infoMessageManager.ShowMessage(message);
        }
        
        /// <summary>
        /// Here the info message at the bottom of the page will be hidden
        /// </summary>
        public void HideInfoMessage()
        {
            infoMessageManager.HideInfoMessage();
        }

        /// <summary>
        /// We change the light sprite to an enabled one
        /// </summary>
        public void TurnOnGeneralLight()
        {
            lightBulbLight.TurnOn();
        }

        /// <summary>
        /// We change the light sprite to a disabled one
        /// </summary>
        public void TurnOffGeneralLight()
        {
            lightBulbLight.TurnOff();
        }

        /// <summary>
        /// Here we will toggle the flash light image of the player HUD
        /// </summary>
        public void TogglePlayerFlashlight()
        {
            flashLightLight.Toggle();
        }

        /// <summary>
        /// When the player opens the chest, this panel will be shown
        /// </summary>
        /// <param name="duration"></param>
        public void ShowBluePotionPanel(float duration)
        {
            StartCoroutine(ShowPanelWithAlphaDelay(bluePotionPanel, duration));
        }
        
        /// <summary>
        /// The blue potion panel will be hidden when the player has accepted
        /// </summary>
        public void HideBluePotionPanel()
        {
            bluePotionPanel.SetActive(false);
        }
        
        /// <summary>
        /// Here the game over panel is displayed but with a little effect of delay
        /// </summary>
        /// <param name="duration"></param>
        public void ShowGameOverPanel(float duration)
        {
            StartCoroutine(ShowPanelWithAlphaDelay(gameOverPanel, duration));
        }
        
        /// <summary>
        /// Here the win panel is displayed but with a little effect of delay
        /// </summary>
        /// <param name="duration"></param>
        public void ShowWinPanel(float duration)
        {
            StartCoroutine(ShowPanelWithAlphaDelay(winPanel, duration));
        }

        /// <summary>
        /// A little coroutine to make a delay effect opening a panel (increasing the alpha number till hits 1)
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        IEnumerator ShowPanelWithAlphaDelay(GameObject panel, float duration)
        {
            panel.SetActive(true);
            CanvasGroup cg = panel.GetComponent<CanvasGroup>();
            cg.alpha = 0f;
            yield return new WaitForSeconds(duration);
            while (cg.alpha <= 1)
            {
                cg.alpha += 0.05f;
                yield return new WaitForSeconds(0.02f);
            }
        }
    }
}
