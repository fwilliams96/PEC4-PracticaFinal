using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Visual
{
    public class MessageManager : MonoBehaviour 
    {
        public Text messageText;
        public GameObject messagePanel;

        private void Start()
        {
            messagePanel.SetActive(false);
        }

        public void ShowMessage(string message, float duration)
        {
            StartCoroutine(ShowWarningMessageCoroutine(message, duration));
        }

        public void ShowMessage(string message)
        {
            CanvasGroup cg = messagePanel.GetComponent<CanvasGroup>();
            cg.alpha = 1f;
            messageText.text = message;
            messagePanel.SetActive(true);
        }

        public void HideInfoMessage()
        {
            messageText.text = "";
            messagePanel.SetActive(false);
        }

        /// <summary>
        /// With this function, the message automatically will disappear after a few seconds (duration)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        IEnumerator ShowWarningMessageCoroutine(string message, float duration)
        {
            messageText.text = message;
            messagePanel.SetActive(true);
            CanvasGroup cg = messagePanel.GetComponent<CanvasGroup>();
            cg.alpha = 1f;
            yield return new WaitForSeconds(duration);
            while (cg.alpha > 0)
            {
                cg.alpha -= 0.05f;
                yield return new WaitForSeconds(0.05f);
            }

            messagePanel.SetActive(false);
        }

    }
}