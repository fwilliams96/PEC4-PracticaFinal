using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Visual
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private Button startGame;
        [SerializeField] private Button creditsButton;
        [SerializeField] private Button backGame;

        private void Start()
        {
            startGame.onClick.AddListener(OnStartGameButtonClicked);
            creditsButton.onClick.AddListener(OnCreditsButtonClicked);
            backGame.onClick.AddListener(OnBackButtonClicked);
        }

        private void OnStartGameButtonClicked()
        {
            SceneManager.LoadScene("Game");
        }
            
        private void OnCreditsButtonClicked()
        {
            SceneManager.LoadScene("Credits");
        }
    
        private void OnBackButtonClicked()
        {
            SceneManager.LoadScene("TitleMenu");
        }
    }
}
