using System;
using System.Reflection;
#if UNITY_EDITOR
using UnityEditorInternal;
#endif
using UnityEngine;

namespace Visual
{
    #if UNITY_EDITOR
    public class SortingLayerUtils
    {
        /// <summary>
        /// This allows to get all the current sorting layers to finally display all these values in a SerializedField
        /// </summary>
        /// <returns></returns>
        public static string[] SortingLayerNames() {
            Type internalEditorUtilityType = typeof(InternalEditorUtility);
            PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
            var sortingLayers = (string[])sortingLayersProperty.GetValue(null, new object[0]);
            return sortingLayers;
        }
    }
    #endif
}
