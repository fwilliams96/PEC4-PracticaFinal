using UnityEngine;

namespace Model
{
    public class GeneralData : MonoBehaviour
    {
    
        public static GeneralData Instance { get; private set; } = null;
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
        }

        [Header("Player")] public GameObject playerPrefab;
    
        [Header("Magic bonfire")] public GameObject magicBonfirePrefab;
        
        [Header("Bonfire")] public GameObject bonfirePrefab;
        
        [Header("Chest keys")] public GameObject chestKeysPrefab;
        
        [Header("Door keys")] public GameObject doorKeysPrefab;
        
        [Header("Wood")] public GameObject woodPrefab;

        [Header("Enemies")] public GameObject[] enemiesPrefab;
    }
}
