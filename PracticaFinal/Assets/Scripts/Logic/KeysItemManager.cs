using UnityEngine;

namespace Logic
{

    public enum KeysType
    {
        Chest,
        Door
    }
    
    public class KeysItemManager : ItemManager
    {
        [SerializeField] private KeysType keysType;
        
        /// <summary>
        /// This is actually a collectible and this class will be used both chest keys and door keys
        /// </summary>
        /// <param name="player"></param>
        protected override void HandlePlayerEnterCollision(GameObject player)
        {
            if (keysType == KeysType.Chest)
            {
                player.GetComponent<PlayerManager>().HasChestKeys = true;
                GameManager.Instance.ShowChestKeys();
            } 
            else if (keysType == KeysType.Door)
            {
                player.GetComponent<PlayerManager>().HasDoorKeys = true;
                GameManager.Instance.ShowDoorKeys();
            }
            AudioManager.Instance.Play("GrabKeys");
            Destroy(gameObject);
            
        }
    }
}
