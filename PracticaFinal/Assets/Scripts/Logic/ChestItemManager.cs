using UnityEngine;

namespace Logic
{
    public class ChestItemManager : ItemManager, Interactable
    {
        private bool _opened = false;

        [SerializeField] private Animator animator;
    
        /// <summary>
        /// When the player tries to interact with the chest but he doesn't have the proper keys a message will be displayed
        /// In case the player has the keys then he we will be able to open it, we save the interactable object inside the player
        /// </summary>
        /// <param name="player"></param>
        protected override void HandlePlayerEnterCollision(GameObject player)
        {
            if (_opened) return;
            PlayerManager playerManager = player.GetComponent<PlayerManager>();
            if (playerManager.HasChestKeys)
            {
                if (!playerManager.HasCurrentInteractable)
                {
                    playerManager.AddCurrentInteractable(this);
                }
            
                // Show message according player can open the chest
                GameManager.Instance.ShowInfoMessage("Vamos a ver que hay en este cofre");
            }
            else
            {
                playerManager.RemoveCurrentInteractable(this);
            
                // Show message according player can't open the chest yet
                GameManager.Instance.ShowInfoMessage("Aún no tienes las llaves para abrir el cofre...");
            }
            
        }

        /// <summary>
        /// If the player moves away from the chest then the message will disappear
        /// </summary>
        /// <param name="player"></param>
        protected override void HandlePlayerExitCollision(GameObject player)
        {
            if (_opened) return;
            player.GetComponent<PlayerManager>().RemoveCurrentInteractable(this);
            GameManager.Instance.HideInfoMessage();
        }
        
        /// <summary>
        /// Implementing the interact function of the interface Interactable
        /// </summary>
        public void Interact()
        {
            OpenChest();
            GameManager.Instance.HideInfoMessage();
            GameManager.Instance.Player.RemoveCurrentInteractable(this);
        }

        /// <summary>
        /// This function will execute the open animation of the chest and additionally it will make a sound
        /// </summary>
        private void OpenChest()
        {
            animator.SetBool("IsOpen", true);
            AudioManager.Instance.Play("OpenChest");
        }

        /// <summary>
        /// Function called at the end of the open animation to show the panel according the player has achieved the blue potion
        /// </summary>
        public void ChestOpened()
        {
            Debug.Log("Chest opened");
            GameManager.Instance.BluePotionFound();
            _opened = true;
        }
    }
}
