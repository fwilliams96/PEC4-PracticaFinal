using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using Model;
using UnityEngine;
using UnityEngine.SceneManagement;
using Visual;

namespace Logic
{
    public class GameManager : MonoBehaviour
    {
        
        public static GameManager Instance { get; private set; } = null;

        [SerializeField] private PlayerManager player;
        [SerializeField] private List<EnemyManager> enemies;
        [SerializeField] private EnemyManager slenderman;
        [SerializeField] private SpriteRenderer cameraBlackMask;
        [SerializeField] private GameCanvasManager gameCanvasManager;
        [SerializeField] private GeneralLightManager generalLightManager;
        
        public bool Playing { get; private set; } = false;

        public PlayerManager Player => player;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void Start()
        {
            if (PlayerPrefs.HasKey("Instructions") && PlayerPrefs.GetInt("Instructions") == 1)
            {
                StartGame();
            }
        }

        /// <summary>
        /// This function is called from the Start() if the player already saw the instructions or by the 'Play' button at the end of the instructions
        /// </summary>
        public void StartGame()
        {
            gameCanvasManager.CloseInstructionsMenu();
            Playing = true;
            EnemySpawner.Instance.StartSpawning();
            AudioManager.Instance.ChangeToGameTheme();
        }
        
        /// <summary>
        /// This function allows to end the game
        /// </summary>
        public void StopGame()
        {
            Playing = false;
            EnemySpawner.Instance.StopSpawning();
            AudioManager.Instance.StopMainTheme();
        }

        
        /// <summary>
        /// This is executed when the player uses the mechanical switch or when Slenderman is hit by the flashlight while he's furious
        /// </summary>
        public void TurnOnGeneralLight(bool slendermanHasActivatedTheLight = false)
        {
            if (generalLightManager.LightEnabled) return;
            Debug.Log("Turn on general light");
            AudioManager.Instance.Play("ToggleGeneralLight");

            if (!slendermanHasActivatedTheLight)
            {
                AudioManager.Instance.Play("MonstersWokenUp");
            }
            
            generalLightManager.TurnOn();
            
            gameCanvasManager.TurnOnGeneralLight();
            ChangeAlpha(cameraBlackMask, 0f);
            MonstersStartChasingPlayer();
            if (!slendermanHasActivatedTheLight)
            {
                SlendermanStartChasingPlayer();
            }
        }

        /// <summary>
        /// Called from the mechanical switch when the player turn it off again 
        /// </summary>
        public void TurnOffGeneralLight()
        {
            if (!generalLightManager.LightEnabled) return;
            Debug.Log("Turn off general light");
            AudioManager.Instance.Play("ToggleGeneralLight");
            
            generalLightManager.TurnOff();
            
            gameCanvasManager.TurnOffGeneralLight();
            ChangeAlpha(cameraBlackMask, 1f);
            MonstersStopChasingPlayer();
            SlendermanStopChasingPlayer();
        }

        /// <summary>
        /// This allows to change the alpha of the black mask to lighten or darken the area
        /// </summary>
        /// <param name="spriteRenderer"></param>
        /// <param name="alpha"></param>
        private void ChangeAlpha(SpriteRenderer spriteRenderer, float alpha)
        {
            spriteRenderer.material.color = new Color(spriteRenderer.color.r, 
                spriteRenderer.color.g, spriteRenderer.color.b, alpha);
        }

        /// <summary>
        /// All the current enemies start to chase the player
        /// </summary>
        private void MonstersStartChasingPlayer()
        {
            enemies.ForEach(enemy => enemy.StartChasingPlayer(false));
        }

        /// <summary>
        /// Slenderman starts to chase the player
        /// </summary>
        private void SlendermanStartChasingPlayer()
        {
            if (slenderman != null)
            {
                slenderman.StartChasingPlayer();    
            }
        }
    
        /// <summary>
        /// All the current enemies stop to chase the player
        /// </summary>
        private void MonstersStopChasingPlayer()
        {
            enemies.ForEach(enemy => enemy.StopChasingPlayer());
            
        }

        /// <summary>
        /// Slenderman stop chasing the player
        /// </summary>
        private void SlendermanStopChasingPlayer()
        {
            if (slenderman != null)
            {
                slenderman.StopChasingPlayer();    
            }
        }

        /// <summary>
        /// Toggle the flash light of the player
        /// </summary>
        public void TogglePlayerFlashlight()
        {
            gameCanvasManager.TogglePlayerFlashlight();
            AudioManager.Instance.Play("ToggleFlashlight");
            player.GetComponent<PlayerManager>().TogglePlayerFlashlight();
            if (!generalLightManager.LightEnabled)
            {
                MonstersStopChasingPlayer();
                SlendermanStopChasingPlayer();
            }
        }

        /// <summary>
        /// Update the wood stat of the player HUD
        /// </summary>
        /// <param name="woodCount"></param>
        public void UpdateWoodCount(int woodCount)
        {
            gameCanvasManager.UpdateWoodCount(woodCount);
        }
    
        /// <summary>
        /// Update the chest keys stat of the player HUD
        /// </summary>
        public void ShowChestKeys()
        {
            gameCanvasManager.ShowChestKeys();
        }
        
        /// <summary>
        /// Update the door keys stat of the player HUD
        /// </summary>
        public void ShowDoorKeys()
        {
            gameCanvasManager.ShowDoorKeys();
        }

        /// <summary>
        /// It allows to place a magic or normal bonfire depending if the player has the blue potion or not
        /// </summary>
        public void PlaceBonfire()
        {
            if (player.WoodCount > 0)
            {
                GameObject bonfire;
                if (player.HasBluePotion)
                {
                    bonfire = GeneralData.Instance.magicBonfirePrefab;
                    player.HasBluePotion = false;
                    gameCanvasManager.HidePotion();
                }
                else
                {
                    bonfire = GeneralData.Instance.bonfirePrefab;
                }
                
                Instantiate(bonfire, player.gameObject.transform.position, Quaternion.identity);
                AudioManager.Instance.Play("PlaceBonfire");
                player.WoodCount -= 1;
                UpdateWoodCount(player.WoodCount);
            }
            else
            {
                // show message
                gameCanvasManager.ShowWarningMessage("¡No tienes madera!", 0.25f);
            }
        
        }

        /// <summary>
        /// It allows to show or hide the pause menu
        /// </summary>
        public void TogglePauseMenu()
        {
            gameCanvasManager.TogglePauseMenu();
            if (gameCanvasManager.PauseMenuEnabled)
            {
                PauseGame();
            }
            else
            {
                ResumeGame();
            }
        }


        /// <summary>
        /// Pause the time scale of the game
        /// </summary>
        private void PauseGame()
        {
            Time.timeScale = 0;
        }

        /// <summary>
        /// Resume the time scale of the game
        /// </summary>
        private void ResumeGame()
        {
            Time.timeScale = 1; 
        }

        /// <summary>
        /// Show a message at the bottom part of the UI
        /// </summary>
        /// <param name="message"></param>
        public void ShowInfoMessage(string message)
        {
            gameCanvasManager.ShowInfoMessage(message);
        }

        /// <summary>
        /// Hide the message at the bottom part of the UI
        /// </summary>
        public void HideInfoMessage()
        {
            gameCanvasManager.HideInfoMessage();
        }

        /// <summary>
        /// It opens the blue potion panel when the player opens the chest
        /// </summary>
        public void BluePotionFound()
        {
            // reproduce sound 
            gameCanvasManager.ShowBluePotionPanel(0.4f);
        }
        
        /// <summary>
        /// It closes the blue potion panel
        /// It updates the keys stat and the potion stat of the player HUD
        /// </summary>
        public void CloseBluePotionPanel()
        {
            gameCanvasManager.HideBluePotionPanel();
            gameCanvasManager.HideKeys();
            
            player.HasBluePotion = true;
            gameCanvasManager.ShowPotion();
        }
        
        /// <summary>
        /// This stops all the important functionalities of the game
        /// It also turns the general light off and the same with the player light
        /// Finally it reproduces a game over sound and shows the game over panel
        /// </summary>
        public void GameOver()
        {
            Debug.Log("Game over");
            StopGame();
            
            // reproduce sound 
            AudioManager.Instance.Play("GameOver");
            
            gameCanvasManager.ShowGameOverPanel(0.4f);
            MonstersStopChasingPlayer();
            SlendermanStopChasingPlayer();
        }
        
        /// <summary>
        /// This stops all the important functionalities of the game
        /// Finally it reproduces a win sound and shows the win panel
        /// </summary>
        public void Win()
        {
            Debug.Log("Win");
            StopGame();
            
            // reproduce sound 
            AudioManager.Instance.Play("Win");
            
            gameCanvasManager.ShowWinPanel(0.4f);
            MonstersStopChasingPlayer();
            SlendermanStopChasingPlayer();
        }

        /// <summary>
        /// This allows to reload the scene
        /// </summary>
        public void RestartGame()
        {
            SceneManager.LoadScene("Game");
        }

        /// <summary>
        /// This will reload the title menu scene to quit the game
        /// </summary>
        public void QuitGame()
        {
            PlayerPrefs.DeleteAll();
            ResumeGame();
            AudioManager.Instance.ChangeToMenuTheme();
            SceneManager.LoadScene("TitleMenu");
        }

        /// <summary>
        /// When Slenderman is angry, a creepy sound will play and a warning will be shown
        /// </summary>
        public void SlendermanIsAngry()
        {
            // play angry sound
            AudioManager.Instance.Play("Slenderman");
            
            // show a warning message to the user
            gameCanvasManager.ShowWarningMessage("¡Slenderman se ha enfadado! Ten cuidado con iluminarlo...", 2f);
        }
        
        /// <summary>
        /// When a new enemy is respawned it will be added here as well
        /// </summary>
        /// <param name="enemyManager"></param>
        public void AddEnemy(EnemyManager enemyManager)
        {
            enemies.Add(enemyManager);
        }

        /// <summary>
        /// When a enemy is destroyed this list has to be updated
        /// </summary>
        /// <param name="enemyManager"></param>
        public void DestroyEnemy(EnemyManager enemyManager)
        {
            enemies.Remove(enemyManager);
            Destroy(enemyManager.gameObject);
        }

        /// <summary>
        /// When Slenderman is destroyed, we set the variable to null and we delete the gameobject as well
        /// </summary>
        /// <param name="enemyManager"></param>
        public void DestroySlenderman(EnemyManager enemyManager)
        {
            if (enemyManager == slenderman)
            {
                slenderman = null;
                Destroy(enemyManager.gameObject);    
            }
        }
        
        /// <summary>
        /// When the player approaches to an interactable object, the player can interact with this one
        /// </summary>
        public void InteractWithCurrentObject()
        {
            if (player.HasCurrentInteractable)
            {
                player.AllowInteraction();
            }
        }

    }
}
