using System;
using UnityEngine;

namespace Logic
{
    [RequireComponent(typeof(AudioSource))]
    public class EnemySoundManager : MonoBehaviour
    {
        private AudioSource _audioSource;
        [SerializeField] protected AudioClip defaultSound;
        [SerializeField] protected AudioClip wokenUpSound;
        [SerializeField] protected AudioClip deathSound;

        protected AudioClip currentAudio;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        void Start()
        {
            currentAudio = defaultSound;
            StartSound();
        }

        /// <summary>
        /// We play the current audio of the enemy 
        /// </summary>
        public void StartSound()
        {
            _audioSource.clip = currentAudio;
            _audioSource.loop = true;
            _audioSource.Play();
        }

        /// <summary>
        /// We stop the current audio of the enemy 
        /// </summary>
        public void StopSound()
        {
            _audioSource.Stop();
        }

        /// <summary>
        /// Reproduce the woken up sound only once and using the general audio instead of the spatial one
        /// This can be overriden if we need different moods like Slenderman has
        /// </summary>
        public virtual void ReproduceWokenUpSound()
        {
            AudioManager.Instance.Play(wokenUpSound);
        }
        
        /// <summary>
        /// Reproduce the death only once and using the general audio instead of the spatial one
        /// </summary>
        public virtual void ReproduceDeathSound()
        {
            AudioManager.Instance.Play(deathSound);
        }
    }
}
