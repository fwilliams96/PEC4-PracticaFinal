using System;
using Model;
using UnityEngine;

namespace Logic
{
    public class AudioManager : MonoBehaviour
    {

        public static AudioManager Instance { get; private set; } = null;
    
        public Sound menuTheme;
        
        public Sound gameTheme;

        public Sound[] effects;

        private AudioSource mainAudioSource;

        private AudioSource secondaryAudioSource;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }
            mainAudioSource = gameObject.AddComponent<AudioSource>();
            secondaryAudioSource = gameObject.AddComponent<AudioSource>();
        }
        
        /// <summary>
        /// We start to reproduce the main theme sound
        /// </summary>
        private void Start()
        {
            ChangeMainTheme(menuTheme);
        }

        /// <summary>
        /// Change to menu theme instead of the game theme
        /// </summary>
        public void ChangeToMenuTheme()
        {
            mainAudioSource.Stop();
            ChangeMainTheme(menuTheme);
        }

        /// <summary>
        /// Change to the game theme instead of the intro theme
        /// </summary>
        public void ChangeToGameTheme()
        {
            mainAudioSource.Stop();
            ChangeMainTheme(gameTheme);
        }
                
        /// <summary>
        /// This allows to change the main theme
        /// </summary>
        /// <param name="mainTheme"></param>
        private void ChangeMainTheme(Sound mainTheme)
        {
            if (mainTheme != null && mainTheme.clip != null)
            {
                mainAudioSource.clip = mainTheme.clip;
                mainAudioSource.loop = true;
                mainAudioSource.Play();
            }
        }

        /// <summary>
        /// We look for the sound from the name parameter and then we reproduce it once using the secondary audio source
        /// </summary>
        /// <param name="name"></param>
        public void Play(string name)
        {
            var sound = Array.Find(effects, sound => sound.name == name);
            if (sound != null && sound.clip != null)
            {
                secondaryAudioSource.PlayOneShot(sound.clip);
            }
        }

        /// <summary>
        /// We can reproduce an audio clip as well by sending it directly here
        /// </summary>
        /// <param name="audioClip"></param>
        public void Play(AudioClip audioClip)
        {
            if (audioClip != null)
            {
                secondaryAudioSource.PlayOneShot(audioClip);
            }
        }

        /// <summary>
        /// Function called from the GameManager when the game is over
        /// </summary>
        public void StopMainTheme()
        {
            mainAudioSource.Stop();
        }
    }
}