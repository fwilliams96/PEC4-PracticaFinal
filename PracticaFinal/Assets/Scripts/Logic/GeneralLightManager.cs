using System;
using UnityEngine;

namespace Logic
{
    public class GeneralLightManager : ItemManager, Interactable
    {
        [SerializeField] private Animator m_Animator;

        public bool LightEnabled { get; private set; } = false;

        private void Start()
        {
            LightEnabled = false;
        }

        /// <summary>
        /// Here the player can interact at anytime with the general light
        /// </summary>
        /// <param name="player"></param>
        protected override void HandlePlayerEnterCollision(GameObject player)
        {
            Debug.Log("Play is staying at general light");
            if (LightEnabled)
            {
                GameManager.Instance.ShowInfoMessage("Vigila, parece ser que la luz está encendida..");
            }
            else
            {
                GameManager.Instance.ShowInfoMessage("La luz está apagada");
            }

            if (!player.GetComponent<PlayerManager>().HasCurrentInteractable)
            {
                player.GetComponent<PlayerManager>().AddCurrentInteractable(this);
            }
        }

        /// <summary>
        /// We remove the message when the player moves away
        /// </summary>
        /// <param name="player"></param>
        protected override void HandlePlayerExitCollision(GameObject player)
        {
            player.GetComponent<PlayerManager>().RemoveCurrentInteractable(this);
            GameManager.Instance.HideInfoMessage();
        }

        /// <summary>
        /// Turn on the general light
        /// </summary>
        public void TurnOn()
        {
            LightEnabled = true;
            ChangeAnimation();
        }

        /// <summary>
        /// Turn on the general light
        /// </summary>
        public void TurnOff()
        {
            LightEnabled = false;
            ChangeAnimation();
        }

        /// <summary>
        /// Change the animation of the general light
        /// </summary>
        private void ChangeAnimation()
        {
            m_Animator.SetBool("Enabled", LightEnabled);
        }

        /// <summary>
        /// When the player interacts with the object, it will do like a toggle and it will notify the game manager to do what the game manager thinks is necessary
        /// </summary>
        public void Interact()
        {
            Debug.Log("Interact with general light");
            Debug.Log($"Enabled: {LightEnabled}");
            if (LightEnabled)
            {
                GameManager.Instance.TurnOffGeneralLight();
            }
            else
            {
                GameManager.Instance.TurnOnGeneralLight();
            }
            LightEnabled = !LightEnabled;
            GameManager.Instance.HideInfoMessage();
        }
    }
}
