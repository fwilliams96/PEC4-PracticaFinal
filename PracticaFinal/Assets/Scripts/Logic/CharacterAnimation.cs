using System.Collections;
using UnityEngine;

namespace Logic
{
    public class CharacterAnimation : MonoBehaviour
    {
        [SerializeField] private Animator m_Animator;
        [SerializeField] private SpriteRenderer m_SpriteRenderer;

        private Color _initialSpriteColor;

        private void Start()
        {
            _initialSpriteColor = m_SpriteRenderer.material.color;
        }

        /// <summary>
        /// Here we send to the animation both horizontal and vertical values from the movement
        /// </summary>
        /// <param name="horizontal"></param>
        /// <param name="vertical"></param>
        public void UpdateMovementParameters(float horizontal, float vertical)
        {
            m_Animator.SetFloat("Horizontal", horizontal);
            m_Animator.SetFloat("Vertical", vertical);
        }
    
        /// <summary>
        /// When the player starts walking this value will allow to switch from the idle blend tree to the walking blend tree
        /// </summary>
        /// <param name="isWalking"></param>
        public void UpdateWalkingParameter(bool isWalking)
        {
            m_Animator.SetBool("IsWalking", isWalking);
        }

        /// <summary>
        /// We start the coroutine to show an injured effect
        /// </summary>
        public void EnableInjuredAnimation()
        {
            StartCoroutine(BlinkSpriteToRed());
        }
    
        /// <summary>
        /// We allow the character to blink from its original color to a red color for a certain time
        /// </summary>
        /// <returns></returns>
        IEnumerator BlinkSpriteToRed()
        {
            for (var n = 0; n < 4; n++)
            {
                m_SpriteRenderer.material.color = new Color(1, 0, 0, _initialSpriteColor.a);
                yield return new WaitForSeconds(0.2f);
                m_SpriteRenderer.material.color = _initialSpriteColor;
                yield return new WaitForSeconds(0.2f);
            }
        }

        /// <summary>
        /// This layer change will be useful for Slenderman to change from angry to calm or vice versa (but keeping the same diagrams).
        /// </summary>
        /// <param name="layerName"></param>
        /// <param name="weight"></param>
        public void UpdateLayerWeight(string layerName, float weight)
        {
            m_Animator.SetLayerWeight(m_Animator.GetLayerIndex(layerName), weight);
        }

    }
}
