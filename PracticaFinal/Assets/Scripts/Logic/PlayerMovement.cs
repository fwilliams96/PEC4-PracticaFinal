using UnityEngine;

namespace Logic
{
    public class PlayerMovement : MonoBehaviour
    {
        
        [SerializeField] private CharacterAnimation characterAnimation;
        [SerializeField] private Rigidbody2D m_Rigidbody2D;
        [SerializeField] private float speed = 4f;
    
        private Vector2 _direction;

        /// <summary>
        /// We get the horizontal and vertical values to finally update the animations and move the player using the rigidbody component
        /// </summary>
        void Update()
        {
            if (!GameManager.Instance.Playing) return;
            
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");

            bool isWalking = horizontal != 0 || vertical != 0;

            if (isWalking)
            {
                characterAnimation.UpdateMovementParameters(horizontal, vertical);
            }
            characterAnimation.UpdateWalkingParameter(isWalking);
        
            _direction = new Vector2(horizontal, vertical).normalized;
        }

        private void FixedUpdate()
        {
            if (!GameManager.Instance.Playing) return;
            m_Rigidbody2D.MovePosition(m_Rigidbody2D.position + _direction * speed * Time.fixedDeltaTime);
        }
    }
}
