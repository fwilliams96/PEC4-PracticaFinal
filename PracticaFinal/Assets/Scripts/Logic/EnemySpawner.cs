using System;
using Model;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Logic
{
    public class EnemySpawner : MonoBehaviour
    {
        public static EnemySpawner Instance { get; private set; } = null;

        [SerializeField] private bool counting = false;
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        [SerializeField] private float timeTillNextRespawn = 3;

        private float _currentTime;
    
        private int _currentEnemyIndex;
        private NavMeshTriangulation _triangulation;
    
        void Start()
        {
            _currentTime = timeTillNextRespawn;
            _currentTime = timeTillNextRespawn;
            _triangulation = NavMesh.CalculateTriangulation();
        }

        void Update()
        {
            if (counting)
            {
                _currentTime -= Time.deltaTime;
                if ( _currentTime <= 0)
                {
                    SpawnNextEnemy();
                    _currentTime = timeTillNextRespawn;
                }

            }
        }

        /// <summary>
        /// This function will randomly create a new enemy in the game
        /// </summary>
        private void SpawnNextEnemy()
        {
            var nextEnemy = GetNextEnemy();

            var validRandomPosition = GetValidRandomPositionInNavMesh();
            var newEnemy = Instantiate(nextEnemy, validRandomPosition, Quaternion.identity);
            GameManager.Instance.AddEnemy(newEnemy.GetComponent<EnemyManager>());
            
            _currentEnemyIndex = (_currentEnemyIndex + 1) % GeneralData.Instance.enemiesPrefab.Length;
        }

        /// <summary>
        /// This function will pick up a valid but random position of the navmesh to be used
        /// </summary>
        /// <returns></returns>
        public Vector3 GetValidRandomPositionInNavMesh()
        {
            int vertexIndex = Random.Range(0, _triangulation.vertices.Length);
            NavMeshHit navMeshHit;
            if (NavMesh.SamplePosition(_triangulation.vertices[vertexIndex], out navMeshHit, 2f, 1))
            {
                return navMeshHit.position;
            }

            Debug.LogWarning("This shouldn't happen, take a look at navmesh!");
            return Vector3.zero;
        }

        /// <summary>
        /// Get the current enemy to instantiate by the index
        /// </summary>
        /// <returns></returns>
        private GameObject GetNextEnemy()
        {
            return GeneralData.Instance.enemiesPrefab[_currentEnemyIndex];
        }
        
        /// <summary>
        /// This will start the spawning functionality
        /// </summary>
        public void StartSpawning()
        {
            counting = true;
            _currentTime = timeTillNextRespawn;
        }
    
        /// <summary>
        /// This will end the spawning functionality
        /// </summary>
        public void StopSpawning()
        {
            counting = false;
            _currentTime = timeTillNextRespawn;
        }

    }
}
