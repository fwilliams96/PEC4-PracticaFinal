using UnityEngine;
using Visual;

namespace Logic
{
    public class FlashlightManager : MonoBehaviour
    {
        private GameObject fieldOfView;
        [SerializeField] private GameObject lightSphere;

        private void Start()
        {
            fieldOfView = GameObject.FindGameObjectWithTag("FieldOfView");
        }

        /// <summary>
        /// The position of the mesh renderer (flash light) will be only updated in case is active
        /// </summary>
        void Update()
        {
            if (!fieldOfView.GetComponent<FieldOfView>().IsActive) return;
            Vector3 targetPosition = GetMouseWorldPosition();
            Vector3 aimDir = (targetPosition - transform.position).normalized;
            fieldOfView.GetComponent<FieldOfView>().SetAimDirection(aimDir);
            fieldOfView.GetComponent<FieldOfView>().SetOrigin(transform.position);
        }

        /// <summary>
        /// Utility function to convert the mouse position to a valid world position (forcing the z value to 0)
        /// </summary>
        /// <returns></returns>
        private Vector3 GetMouseWorldPosition() {
            Vector3 vec = GetMouseWorldPositionWithZ(Input.mousePosition, Camera.main);
            vec.z = 0f;
            return vec;
        }
    
        /// <summary>
        /// Here we convert from screen coordinates to a world coordinates using the camera
        /// </summary>
        /// <param name="screenPosition"></param>
        /// <param name="worldCamera"></param>
        /// <returns></returns>
        private Vector3 GetMouseWorldPositionWithZ(Vector3 screenPosition, Camera worldCamera) {
            Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
            return worldPosition;
        }
    
        /// <summary>
        /// Toggle the player light (mesh renderer) and updating the sphere which is actually also a mesh renderer (but static)
        /// </summary>
        public void TogglePlayerFlashlight()
        {
            fieldOfView.GetComponent<FieldOfView>().ToggleFieldOfView();
            lightSphere.SetActive(fieldOfView.GetComponent<FieldOfView>().IsActive);
        }
    
    }
}
