using System;
using System.Collections;
using Model;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Logic
{
    public class EnemyManager : MonoBehaviour
    {
        [SerializeField] protected EnemyMovement enemyMovement;
        [SerializeField] protected CharacterAnimation characterAnimation;
        [SerializeField] protected EnemySoundManager enemySoundManager;

        protected bool dead = false;

        private void Start()
        {
            SetPlayerReachedListener();
        }

        protected void SetPlayerReachedListener()
        {
            // We start listening to the player reached event from enemy movement
            enemyMovement.PlayerReached += KillPlayer;
        }

        /// <summary>
        /// Function called when PlayerReached event is activated from enemy movement
        /// </summary>
        private void KillPlayer()
        {
            Debug.Log("Enemy reached player");
            StopChasingPlayer();
            GameManager.Instance.GameOver();
        }

        /// <summary>
        /// We play the particar sound of the enemy when this one is woken up
        /// Then we change the layer mask to stop colliding with them with our flash light
        /// Finally, they start chasing the player using the navmesh agent
        /// </summary>
        public virtual void StartChasingPlayer(bool reproduceSound = true)
        {
            if (enemyMovement.FollowingPlayer || dead) return;
            Debug.Log("StartChasingPlayer");
            if (reproduceSound)
            {
                enemySoundManager.ReproduceWokenUpSound();
            }
            enemySoundManager.StopSound();
            ChangeLayerToGameObjectAndAllChildren(LayerMask.NameToLayer("Default"));
            enemyMovement.StartChasingPlayer(GameManager.Instance.Player.transform);
        }
    
        /// <summary>
        /// Here we don't reproduce any sound
        /// We change the layer mask back to the original one and then we tell them to stop chasing the player
        /// </summary>
        public virtual void StopChasingPlayer()
        {
            ChangeLayerToGameObjectAndAllChildren(LayerMask.NameToLayer("BehindMask"));
            enemyMovement.StopChasingPlayer();
            enemySoundManager.StartSound();
        }

        /// <summary>
        /// We change both the parent's and the children's layer
        /// </summary>
        /// <param name="newLayer"></param>
        private void ChangeLayerToGameObjectAndAllChildren(int newLayer)
        {
            gameObject.layer = newLayer;
            foreach (var trans in gameObject.GetComponentsInChildren<Transform>(true))
            {
                trans.gameObject.layer = newLayer;
            }
        }

        /// <summary>
        /// Here the general function to destroy our enemies, they will display an injured animation and finally they will drop some loot
        /// </summary>
        public virtual void DestroyEnemy()
        {
            if (dead) return;
            dead = true;
            Debug.Log("Destroy enemy");
            characterAnimation.EnableInjuredAnimation();

            enemySoundManager.ReproduceDeathSound();
            StartCoroutine(DropLoot());

        }

        /// <summary>
        /// Depending on the player's items they will drop a different thing (or maybe nothing)
        /// Finally we call to GameManager to update the list of enemies as well
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerator DropLoot()
        {
            yield return new WaitForSeconds(1.6f);
            if (!GameManager.Instance.Player.HasChestKeys)
            {
                if (Random.Range(0, 2) == 0)
                {
                    Instantiate(GeneralData.Instance.chestKeysPrefab, transform.position, transform.rotation);
                    Debug.Log("You are lucky, chest keys dropped!");
                }
                else
                {
                    Instantiate(GeneralData.Instance.woodPrefab, transform.position, transform.rotation);
                    Debug.Log("Wood dropped!");
                }
            }
            else
            {
                if (Random.Range(0, 2) == 0)
                {
                    Instantiate(GeneralData.Instance.woodPrefab, transform.position, transform.rotation);
                    Debug.Log("Wood dropped!");
                }
            }

            GameManager.Instance.DestroyEnemy(this);
        }

    }
}
