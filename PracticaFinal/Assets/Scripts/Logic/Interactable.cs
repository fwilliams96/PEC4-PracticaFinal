namespace Logic
{
    /// <summary>
    /// A useful interface for all the interactable objects
    /// </summary>
    public interface Interactable
    {
        public void Interact();
    }
}
