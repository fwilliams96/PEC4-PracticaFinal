using System;
using UnityEngine;

namespace Logic
{
    /// <summary>
    /// A useful class to group all the important collision events
    /// The targets will be both player and enemies
    /// </summary>
    public class ItemManager : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                HandlePlayerEnterCollision(other.gameObject);    
            }

            if (other.CompareTag("Enemy"))
            {
                HandleEnemyEnterCollision(other.gameObject);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                HandlePlayerExitCollision(other.gameObject);    
            }

            if (other.CompareTag("Enemy"))
            {
                HandleEnemyExitCollision(other.gameObject);
            }
            
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                HandlePlayerEnterCollision(other.gameObject);    
            }

            if (other.gameObject.CompareTag("Enemy"))
            {
                HandleEnemyEnterCollision(other.gameObject);
            }
            
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                HandlePlayerExitCollision(other.gameObject);    
            }

            if (other.gameObject.CompareTag("Enemy"))
            {
                HandleEnemyExitCollision(other.gameObject);
            }
            
        }

        protected virtual void HandlePlayerEnterCollision (GameObject player) {}
    
        protected virtual void HandlePlayerExitCollision (GameObject player) {}
        
        protected virtual void HandleEnemyEnterCollision (GameObject enemy) {}
    
        protected virtual void HandleEnemyExitCollision (GameObject enemy) {}
        
    }
}
