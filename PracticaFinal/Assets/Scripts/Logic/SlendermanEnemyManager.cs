using System.Collections;
using Model;
using UnityEngine;

namespace Logic
{
    public class SlendermanEnemyManager : EnemyManager
    {
        public enum SlendermanMode
        {
            Calm,
            Angry
        }
    
        [SerializeField] private float timeTillNextMove = 30f;
        [SerializeField] private float timeTillNextHumorChange = 60f;

        private bool _countingRandomMovement;
        private float _currentRandomMovementTime;

        private bool _countingMoodChange;
        private float _currentMoodChangeTime;
        private SlendermanMode _slendermanMode;

        void Start()
        {
            _slendermanMode = SlendermanMode.Calm; 
        
            _countingRandomMovement = true;
            _currentRandomMovementTime = timeTillNextMove;
        
            _countingMoodChange = true;
            _currentMoodChangeTime = timeTillNextHumorChange;
            SetPlayerReachedListener();
        }

        /// <summary>
        /// Here Slenderman will move randomly across the level picking up a valid position from the navmesh
        /// Every time the counter timeTillNextHumorChange arrives to 0, Slenderman will change his mood
        /// </summary>
        private void Update()
        {
            if (!GameManager.Instance.Playing) return;
            if (_countingRandomMovement)
            {
                _currentRandomMovementTime -= Time.deltaTime;
                if ( _currentRandomMovementTime <= 0)
                {
                    NextSlendermanMovement();
                    _currentRandomMovementTime = timeTillNextMove;
                }
            }

            if (_countingMoodChange)
            {
                _currentMoodChangeTime -= Time.deltaTime;
                if ( _currentMoodChangeTime <= 0)
                {
                    ChangeSlendermanMood();
                    _currentMoodChangeTime = timeTillNextHumorChange;
                }
            }
        }

        /// <summary>
        /// He will stop moving randomly to start chasing the player but depending on the move Slenderman can turn on the general light 
        /// </summary>
        public override void StartChasingPlayer(bool reproduceSound = true)
        {
            if (!enemyMovement.FollowingPlayer)
            {
                Debug.Log("Slenderman has started to chase the player");
                StopRandomMovementTimer();
                base.StartChasingPlayer(reproduceSound);

                if (_slendermanMode == SlendermanMode.Angry)
                {
                    GameManager.Instance.TurnOnGeneralLight(true);
                }
                else if (_slendermanMode == SlendermanMode.Calm)
                {
                    StopMoodChangeTimer();
                }
            }
        }

        /// <summary>
        /// In case the player turn the flash light off, Slenderman will do different things depending on the current mood
        /// If Slenderman is angry, he will continue chasing the player
        /// If Slenderman is calm, then he will stop chasing the player and he will start his random patrol again 
        /// </summary>
        public override void StopChasingPlayer()
        {
            // Even if the player turn off the light
            // Slenderman will keep following him till the next mood change
            // If we already reached him (game over), despite being angry, we force him to stop
            if (enemyMovement.FollowingPlayer && _slendermanMode == SlendermanMode.Angry)
            {
                // play laugh sound
                AudioManager.Instance.Play("SlendermanLaughs");
            }
            else if (!enemyMovement.FollowingPlayer || _slendermanMode == SlendermanMode.Calm)
            {
                base.StopChasingPlayer();
                StartRandomMovementTimer();
                StartMoodChangeTimer();
            }
        }

        private void NextSlendermanMovement()
        {
            var validRandomPosition = EnemySpawner.Instance.GetValidRandomPositionInNavMesh();
            enemyMovement.StartChasing(validRandomPosition);
        }

        /// <summary>
        /// When Slenderman change his mood we update the animation layer
        /// If Slenderman was following the player because he was angry, when this function is executed then Slenderman will stop following him
        /// </summary>
        private void ChangeSlendermanMood()
        {
            Debug.Log("ChangeSlendermanMood");
            if (_slendermanMode == SlendermanMode.Angry)
            {
                ((SlendermanSoundManager)enemySoundManager).Angry = false;
                _slendermanMode = SlendermanMode.Calm;
                if (enemyMovement.FollowingPlayer)
                {
                    base.StopChasingPlayer();
                    StartRandomMovementTimer();
                }
                characterAnimation.UpdateLayerWeight("AngryLayer", 0f);
                characterAnimation.UpdateLayerWeight("CalmLayer", 1f);

            }
            else if (_slendermanMode == SlendermanMode.Calm)
            {
                ((SlendermanSoundManager)enemySoundManager).Angry = true;
                _slendermanMode = SlendermanMode.Angry;
            
                GameManager.Instance.SlendermanIsAngry();
            
                characterAnimation.UpdateLayerWeight("CalmLayer", 0f);
                characterAnimation.UpdateLayerWeight("AngryLayer", 1f);
            }
        }

        private void StopRandomMovementTimer()
        {
            _countingRandomMovement = false;
            _currentRandomMovementTime = 0;
        }

        private void StartRandomMovementTimer()
        {
            _countingRandomMovement = true;
            _currentRandomMovementTime = timeTillNextMove;
        }

        private void StopMoodChangeTimer()
        {
            _countingMoodChange = false;
            _currentMoodChangeTime = 0;
        }

        private void StartMoodChangeTimer()
        {
            _countingMoodChange = true;
            _currentMoodChangeTime = timeTillNextHumorChange;
        }

        /// <summary>
        /// Slenderman will be killed by the magic bonfire only if he is calm, otherwise, the player will have to wait till he's calm again to try it
        /// </summary>
        public override void DestroyEnemy()
        {
            if (dead) return;
            Debug.Log("Trying to destroy slenderman");
            Debug.Log($"Slenderman mood: {_slendermanMode}");
            // you can only destroy slenderman if he's in the calm mode
            if (_slendermanMode == SlendermanMode.Calm)
            {
                Debug.Log("Destroy slenderman");
                base.DestroyEnemy();
            }
            else
            {
                AudioManager.Instance.Play("SlendermanLaughs");
            }
        }

        /// <summary>
        /// He will drop the final keys to get out the level
        /// </summary>
        /// <returns></returns>
        protected override IEnumerator DropLoot()
        {
            yield return new WaitForSeconds(2f);
            Instantiate(GeneralData.Instance.doorKeysPrefab, transform.position, transform.rotation);
            Debug.Log("Door keys dropped!");
            GameManager.Instance.DestroySlenderman(this);
        }
    }
}
