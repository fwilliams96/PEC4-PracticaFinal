using UnityEngine;

namespace Logic
{
    public class SlendermanSoundManager : EnemySoundManager
    {
        [SerializeField] protected AudioClip angryDefaultSound;
        [SerializeField] protected AudioClip angryWokenUpSound;

        private bool _angry;
        
        /// <summary>
        /// In case Slenderman gets angry the sounds will change to others
        /// </summary>
        public bool Angry
        {
            get => _angry;
            set
            {
                _angry = value;
                StopSound();
                if (_angry)
                {
                    currentAudio = angryDefaultSound;
                }
                else
                {
                    currentAudio = defaultSound;
                }
                StartSound();
            }
        }

        /// <summary>
        /// Override the woken up function to make a different sound depending on Slenderman's mood
        /// </summary>
        public override void ReproduceWokenUpSound()
        {
            if (Angry)
            {
                AudioManager.Instance.Play(angryWokenUpSound);
            }
            else
            {
                AudioManager.Instance.Play(wokenUpSound);

            }
        }
    }
}
