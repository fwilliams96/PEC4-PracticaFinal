using UnityEngine;

namespace Logic
{
    public class EndDoorManager : ItemManager, Interactable
    {
        [SerializeField] private Animator m_Animator;
        
        /// <summary>
        /// When the player tries to interact with the door but he doesn't have the proper keys a message will be displayed
        /// In case the player has the keys then he we will be able to open it, we save the interactable object inside the player
        /// </summary>
        /// <param name="player"></param>
        protected override void HandlePlayerEnterCollision(GameObject player)
        {
            if (player.GetComponent<PlayerManager>().HasDoorKeys)
            {
                GameManager.Instance.ShowInfoMessage("Es hora de irse de aquí...");
                if (!player.GetComponent<PlayerManager>().HasCurrentInteractable)
                {
                    player.GetComponent<PlayerManager>().AddCurrentInteractable(this);
                }
            }
            else
            {
                GameManager.Instance.ShowInfoMessage("Parece que aún no tenemos las llaves de esta puerta");
            }
        }

        /// <summary>
        /// If the player moves away from the chest then the message will disappear
        /// </summary>
        /// <param name="player"></param>
        protected override void HandlePlayerExitCollision(GameObject player)
        {
            player.GetComponent<PlayerManager>().RemoveCurrentInteractable(this);
            GameManager.Instance.HideInfoMessage();
        }

        
        /// <summary>
        /// Implementing the interact function of the interface Interactable
        /// </summary>
        public void Interact()
        {
            OpenDoor();
            GameManager.Instance.HideInfoMessage();
            GameManager.Instance.Player.RemoveCurrentInteractable(this);
        }

        /// <summary>
        /// This function will execute the open animation of the door and additionally it will make a sound
        /// Finally the win panel will be shown due to the end of the game
        /// </summary>
        private void OpenDoor()
        {
            m_Animator.SetBool("IsOpen", true);
            AudioManager.Instance.Play("OpenDoor");
        }

        /// <summary>
        /// This function is called from an animation event from the door animation
        /// </summary>
        private void DoorOpened()
        {
            Debug.Log("Door opened");
            GameManager.Instance.Win();
        }
    }
}
