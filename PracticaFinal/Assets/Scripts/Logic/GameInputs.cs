using UnityEngine;

namespace Logic
{
    public class GameInputs : MonoBehaviour
    {
        /// <summary>
        /// This Update will take care of all the game inputs that are not directly related to the main character
        /// </summary>
        private void Update()
        {
            if (!GameManager.Instance.Playing) return;
            if (Input.GetKeyDown(KeyCode.E))
            {
                // open chest and door (interact with objects)
                GameManager.Instance.InteractWithCurrentObject();
            }
        
            if (Input.GetKeyDown(KeyCode.Space))
            {
                // place bonfire
                GameManager.Instance.PlaceBonfire();
            }
        
            if (Input.GetMouseButtonDown(1))
            {
                // toggle flashlight
                GameManager.Instance.TogglePlayerFlashlight();
            }
            
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // open pause menu
                GameManager.Instance.TogglePauseMenu();
            }

        }

    }
}
