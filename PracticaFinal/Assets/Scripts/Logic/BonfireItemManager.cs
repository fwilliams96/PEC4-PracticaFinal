using System;
using UnityEngine;

namespace Logic
{
    public class BonfireItemManager : ItemManager
    {
    
        [SerializeField] private float timeTillMediumFire = 60f; // seconds
        [SerializeField] private float totalTime = 120f; // seconds
        [SerializeField] private GameObject lightSphere;
        [SerializeField] private CircleCollider2D circleCollider;
    
        [SerializeField] private Animator animator;
    
        private bool _alive;
        private bool _mediumFire;

        private void Start()
        {
            _alive = true;
            _mediumFire = false;
        }

        /// <summary>
        /// This bonfire will decrease its range till it disappears
        /// </summary>
        void Update()
        {
            if (!_alive) return;
            totalTime -= Time.deltaTime;
            if (!_mediumFire && totalTime <= timeTillMediumFire)
            {
                lightSphere.transform.localScale = new Vector3(lightSphere.transform.localScale.x / 2,
                    lightSphere.transform.localScale.y / 2, lightSphere.transform.localScale.z);
                circleCollider.radius = circleCollider.radius / 2;
                _mediumFire = true;
                animator.SetBool("MediumFire", true);
            }

            if (totalTime <= 0)
            {
                _alive = false;
                animator.SetBool("Extinct", true);
                lightSphere.SetActive(false);
                circleCollider.enabled = false;
            }
        }

        /// <summary>
        /// Destroy the enemy only when it's not Slenderman. In that case a normal bonfire doesn't work
        /// </summary>
        /// <param name="enemy"></param>
        protected override void HandleEnemyEnterCollision(GameObject enemy)
        {
            if (enemy.GetComponent<EnemyManager>().GetType() != typeof(SlendermanEnemyManager))
            {
                enemy.GetComponent<EnemyManager>().DestroyEnemy();
            }
        }
    }
}
