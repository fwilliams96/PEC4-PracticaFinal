using UnityEngine;

namespace Logic
{
    public class WoodItemManager : ItemManager
    {
        /// <summary>
        /// This is actually a collectible to allow the player to place bonfires
        /// </summary>
        /// <param name="player"></param>
        protected override void HandlePlayerEnterCollision(GameObject player)
        {
            player.GetComponent<PlayerManager>().WoodCount++;
            GameManager.Instance.UpdateWoodCount(player.GetComponent<PlayerManager>().WoodCount);
            AudioManager.Instance.Play("GrabWood");
            Destroy(gameObject);
        }
    }
}
