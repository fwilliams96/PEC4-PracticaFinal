using System;
using UnityEngine;
using UnityEngine.AI;

namespace Logic
{
    public class EnemyMovement : MonoBehaviour
    {
        [SerializeField] private CharacterAnimation characterAnimation;
        [SerializeField] private NavMeshAgent navMeshAgent;

        public bool HasGoal { get; private set; } = false;

        private Transform _playerTransform;
        private Vector3 _goalPosition;

        public bool FollowingPlayer { get; set; } = false;
        
        public delegate void PlayerReachedEventHandler();
        public event PlayerReachedEventHandler PlayerReached;
    
        private void Start()
        {
            navMeshAgent.updateRotation = false;
            navMeshAgent.updateUpAxis = false;
            FollowingPlayer = false;
        }

        /// <summary>
        /// The enemy saves the transform of the player (which is actually a reference, not a constant value), so in this way the enemy will be able to chase him wherever the player goes
        /// </summary>
        /// <param name="playerTransform"></param>
        public void StartChasingPlayer(Transform playerTransform)
        {
            FollowingPlayer = true;
            _playerTransform = playerTransform;
            StartChasing(playerTransform.position);
        }

        /// <summary>
        /// In case the enemy starts following a specific coordinate (like Slenderman moving randomly across the level)
        /// </summary>
        /// <param name="goal"></param>
        public void StartChasing(Vector3 goal)
        {
            HasGoal = true;
            _goalPosition = goal;
            navMeshAgent.isStopped = false;
        }
        
        /// <summary>
        /// We stop following the player by stopping the navmesh execution
        /// </summary>
        public void StopChasingPlayer()
        {
            FollowingPlayer = false;
            _playerTransform = null;
            StopChasing();
        }

        private void StopChasing()
        {
            HasGoal = false;
            navMeshAgent.isStopped = true;
        }

        private void Update()
        {
            bool isWalking = !navMeshAgent.isStopped && HasGoal;
            if (isWalking)
            {
                if (FollowingPlayer)
                {
                    navMeshAgent.SetDestination(_playerTransform.position);
                }
                else
                {
                    navMeshAgent.SetDestination(_goalPosition);
                }

                var goalReached = CheckEnemyReachedGoal();

                if (FollowingPlayer && goalReached)
                {
                    // If the enemy was following the player and he achieved his goal, then we activate the PlayerReached event
                    if (PlayerReached != null)
                    {
                        FollowingPlayer = false;
                        PlayerReached();
                    }
                }
                isWalking = !goalReached;

                var currentVelocity = navMeshAgent.desiredVelocity.normalized;
            
                characterAnimation.UpdateMovementParameters(currentVelocity.x, currentVelocity.y);
                
            }
            characterAnimation.UpdateWalkingParameter(isWalking);
            
        }
        
        private bool CheckEnemyReachedGoal()
        {
            if (!navMeshAgent.pathPending)
            {
                if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
                {
                    Debug.Log("Enemy reached the goal");
                    return true;
                }
            }
            return false;
        }
    }
}
