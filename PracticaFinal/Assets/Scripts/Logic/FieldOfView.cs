﻿using Logic;
using UnityEngine;
using Visual;

namespace Logic
{
    public class FieldOfView : MonoBehaviour
    {

        [SerializeField] 
        private LayerMask collisionBlocksLayerMask;
    
        [SerializeField] 
        private LayerMask collisionEnemiesLayerMask;
    
        #if UNITY_EDITOR
        [SerializeField] [StringInList(typeof(SortingLayerUtils), "SortingLayerNames")]
        #endif
        private string sortingLayerName;
    
        [SerializeField] 
        private int sortingOrder;
    
        [SerializeField]
        private float fov = 90f; 
    
        [SerializeField]
        private float viewDistance = 8f;
    
        private Mesh mesh;
    
        private Vector3 origin;
        private float startingAngle;

        private bool active = true;

        public bool IsActive => active;

        private void Start() {
            mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = mesh;
        
            origin = Vector3.zero;

            GetComponent<MeshRenderer>().sortingLayerName = sortingLayerName;
            GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
        }

        /// <summary>
        /// This function creates a mesh to simulate the flash light of the player (by Code Monkeys tutorial)
        /// If any ray detects a block then the ray will be cut
        /// If any ray detects an enemy, then the enemy will wake up and they will start to chase the player
        /// </summary>
        private void Update()
        {
            if (!active) return;
        
            int rayCount = 50;
            float angle = startingAngle;
            float angleIncrease = fov / rayCount;

            Vector3[] vertices = new Vector3[rayCount + 1 + 1];
            Vector2[] uv = new Vector2[vertices.Length];
            int[] triangles = new int[rayCount * 3];

            vertices[0] = origin;

            int vertexIndex = 1;
            int triangleIndex = 0;
            for (int i = 0; i <= rayCount; i++) {

                // Enemies can wake up by the flash light only if game has started
                if (GameManager.Instance.Playing)
                {
                    // raycast enemy
                    RaycastHit2D[] raycastHit2DEnemies = Physics2D.RaycastAll(origin, GetVectorFromAngle(angle), viewDistance, collisionEnemiesLayerMask);
            
                    foreach (var raycastHit2DEnemy in raycastHit2DEnemies)
                    {
                        if (raycastHit2DEnemy.collider != null)
                        {
                            raycastHit2DEnemy.collider.gameObject.GetComponent<EnemyManager>().StartChasingPlayer();
                        }
                    }
                }
            
                Vector3 vertex;
                RaycastHit2D raycastHit2D = Physics2D.Raycast(origin, GetVectorFromAngle(angle), viewDistance, collisionBlocksLayerMask);
                if (raycastHit2D.collider == null) {
                    // No hit
                    vertex = origin + GetVectorFromAngle(angle) * viewDistance;
                } else {
                    // Hit object
                    vertex = raycastHit2D.point;
                }
                vertices[vertexIndex] = vertex;

                if (i > 0) {
                    triangles[triangleIndex + 0] = 0;
                    triangles[triangleIndex + 1] = vertexIndex - 1;
                    triangles[triangleIndex + 2] = vertexIndex;

                    triangleIndex += 3;
                }

                vertexIndex++;
                angle -= angleIncrease;
            }


            mesh.vertices = vertices;
            mesh.uv = uv;
            mesh.triangles = triangles;
            mesh.bounds = new Bounds(origin, Vector3.one * 1000f);
        }
    
        /********************** UTILITY FUNCTIONS TO CREATE THE MESH **********************************/
        private Vector3 GetVectorFromAngle(float angle) {
            // angle = 0 -> 360
            float angleRad = angle * (Mathf.PI/180f);
            return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
        }
        
        private float GetAngleFromVectorFloat(Vector3 dir) {
            dir = dir.normalized;
            float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            if (n < 0) n += 360;

            return n;
        }
        /**********************************************************************************************/

        public void SetOrigin(Vector3 origin) {
            this.origin = origin;
        }

        public void SetAimDirection(Vector3 aimDirection) {
            startingAngle = GetAngleFromVectorFloat(aimDirection) + fov / 2f;
        }
        
        /// <summary>
        /// It makes the mesh disappear in case the player turns the flash light off
        /// It also stops the rays calculation
        /// </summary>
        public void ToggleFieldOfView()
        {
            active = !active;
            gameObject.SetActive(active);
        }

    }
}
