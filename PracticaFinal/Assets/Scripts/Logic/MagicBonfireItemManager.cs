using UnityEngine;

namespace Logic
{
    public class MagicBonfireItemManager : ItemManager
    {
        /// <summary>
        /// This bonfire is eternal, so it won't be extinct
        /// Unlike the normal one, this one will allow to destroy Slenderman in case he's not angry (but also it will destroy all the rest of enemies)
        /// </summary>
        /// <param name="enemy"></param>
        protected override void HandleEnemyEnterCollision(GameObject enemy)
        {
            enemy.GetComponent<EnemyManager>().DestroyEnemy();
        }
    }
}
