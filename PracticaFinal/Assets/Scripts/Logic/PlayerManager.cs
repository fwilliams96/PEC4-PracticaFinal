using System;
using UnityEngine;

namespace Logic
{
    public class PlayerManager : MonoBehaviour
    {
        [SerializeField] private FlashlightManager flashlightManager;
        
        private Interactable _currentInteractable = null;

        public bool HasChestKeys { get; set; } = false;

        public bool HasDoorKeys { get; set; } = false;
        
        public bool HasBluePotion { get; set; } = false;

        public int WoodCount { get; set; } = 0;

        public bool HasCurrentInteractable => _currentInteractable != null;

        public void TogglePlayerFlashlight()
        {
            flashlightManager.TogglePlayerFlashlight();
        }

        public void AddCurrentInteractable(Interactable interactable)
        {
            _currentInteractable = interactable;
        }
    
        public void RemoveCurrentInteractable(Interactable interactable)
        {
            if (_currentInteractable == interactable)
            {
                _currentInteractable = null;
            }
        }

        public void AllowInteraction()
        {
            _currentInteractable.Interact();
        }
        
    }
}
