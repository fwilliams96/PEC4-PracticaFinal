# Introducción
Bienvenidos a la última práctica de la asignatura Programación 2D

# Descripción del videojuego y cómo jugar
En este videojuego de formato libre (que hemos aprovechado para llevar a un formato de misterio) se introduce la historia de Dexter, un chico que ha crecido con la leyenda de Slenderman. La leyenda cuenta que se trata de un ser sobrenatural que a menudo se aparece a la gente mediante portales y que trae consigo más seres de este tipo.
El videojuego empieza en el punto en el que se ve al personaje en un sitio totalmente oscuro y en el cual existen diversos monstruos que debe evitar que lo atrapen.
El movimiento del jugador será mediante las teclas A, W, D y S (para las cuatro direcciones), se hará uso del ratón para manejar la linterna (con clic derecho para encenderla), de la tecla E para interactuar con los diferentes elementos, la tecla Escape para mostrar/ocultar el menú pausa y la tecla Espacio para colocar fogatas.
Para matar a los enemigos tendremos que coger madera que se encontrará repartida por el nivel y que los enemigos también podrán soltar al ser eliminados, esta madera será la que nos permitirá poner fogatas y que será la que elimine a los monstruos.
El objetivo final es conseguir salir del sitio mediante la puerta que se nos muestra al principio pero para la cual ha de seguir unos ciertos pasos que contaremos más adelante.

# Implementación del videojuego
## Funcionalidades implementadas
Los puntos básicos consistían en añadir el logo corporativo (ficticio), la pantalla de título, la de menú principal, la de créditos y dos pantallas de juegos.
Respecto las pantallas de juego tenemos tanto la del propio juego como la del menú pausa, además de varias otras pantallas para instrucciones, etc.
Ya que los objetivos eran sencillos, nos hemos centrado en añadir distintas funcionalidades en nuestro videojuego 2D para añadirle más dificultad al reto.
De todo esto, al final del documento se comentarán las dificultades más destacadas que se han encontrado.

## Estructura
La organización de los scripts se ha mantenido tal cual como en la práctica anterior, como modelo-vista-controlador, de las cuales explicaremos una a una los scripts involucrados:

### Capa modelo 
Se mantiene el script **Sound** para facilitar la creación de sonidos desde el inspector del manager de sonidos que comentaremos a continuación.
Además, se ha añadido el script **GeneralData** (como Singleton) para guardar todo los datos del videojuego que fueran globales. En este caso hemos guardado el prefab de las bolas de fuego. De esta forma facilitamos el acceso a todo el resto de scripts.
Estos dos scripts resultan muy útiles a la hora de manejar este tipo de datos tan estático y necesario de centralizar.

### Capa lógica
En este videojuego, hay que destacar que se deben controlar los siguientes elementos:
- Lógica del videojuego en sí
- Protagonista
- Enemigos
- Objetos

Empezando por la lógica central del videojuego, se concentra en el script **GameManager**, este script es el núcleo de la capa de control y se encarga de conectar la capa de vista con la capa de lógica en ambas direcciones.
 
Para el personaje hay que tener en cuenta tanto el movimiento como la lógica propia del personaje. El personaje es gobernado mediante los siguientes tres scripts:
- **PlayerMovement**: Es el script que lee las entradas del usuario para determinar en la dirección que debe moverse el personaje. Además hace uso del script **CharacterAnimation** para actualizar las animaciones.
- **CharacterAnimation**: Para delegar el control propio del animator de los enemigos y del protagonista se ha decidido crear una clase común para ambos.
- **PlayerManager**: Almacena la información importante del personaje y expone el control de la linterna al exterior. 
Como otros scripts, pero que más gobernar al personaje, gobiernan la linterna tenemos los siguientes:
- **FlashlighManager**: Controla tanto la malla encargada de generar el arco de luz de la lintera como de la esfera (que también se trata de una malla) para de esta forma tener estos dos elementos sincronizados y centralizados en un único script.
- **FieldOfView**: Este script es extraído de un tutorial de Code Monkeys y es el encargado de generar el arco de luz gracias al uso de distintos rayos. Este arco o la luz de la linterna será gobernado por script anterior que le dirá la posición en la que aparecer (en la dirección en la que se encuentra el ratón en este caso). De los rayos generados, además es aquí donde comprobamos las colisiones con los enemigos y en caso de suceder, el enemigo será notificado mediante el uso del script **EnemyManager**.

En lo que respecta a los enemigos, tenemos también otros tres grandes scripts (cuatro contando el anterior **CharacterAnimation**):
- **EnemyMovement**: Es el script encargado de perseguir al personaje en caso de que sea despertado por la linterna del jugador (o de la luz general). Para el movimiento hace uso del asset navmesh 2D que viene de la mano del usuario h8man de Github. Es un asset muy útil para videojuegos 2D y que queda muy bien para añadirle un poco de complejidad e inteligencia a los enemigos. También hace uso del **CharacterAnimation** comentado anteriormente para actualizar las animaciones.
- **EnemyManager**: Controla y determina el resultado de las colisiones con el jugador, se encarga de soltar el loot al morir, y es el trigger que permite al enemigo empezar a seguir al jugador o parar de perseguirlo. También controla los sonidos del enemigo mediante el uso del script **EnemySoundManager**.
- **EnemySoundManager**: Se encarga de reproducir los distintos sonidos propios del enemigo (permitiendo al usuario elegir qué sonido usará para cada acción) y además expondrá estas funciones para ser sobreescritas en caso de escalar el funcionamiento.

En el caso de Slenderman, se ha optado por crear subclases que heredasen de los scripts anteriores, en este caso los siguientes dos scripts:
- **SlendermanEnemyManager**: A diferencia de los enemigos anteriores, Slenderman sí está en constante movimiento y esto es gracias a que de forma aleatoria va cogiendo diferentes puntos del mapa a los que ir, usando la malla del asset navmesh 2D para obtenerlos.
- **SlendermanSoundManager**: Slenderman tendrá dos estados distintos: calmado y enfadado. Por lo cual emitirá ruidos distintos en función de su estado a la hora de realizar una acción (ser alarmado por el jugador, etc).

También relativo a los enemigos, tenemos el script **EnemySpawner**, que tal y como dice el nombre se encargará de instanciar nuevos enemigos (excepto Slenderman) cada cierto tiempo en puntos aleatorios del mapa.
Para hacer esto, usa también la malla 2D generada por el asset navmesh 2D, que permitirá añadirle un punto de realismo más.

Respecto los items, tenemos tanto los que corresponden a loot, los que pueden ser colocados por el jugador y los elementos interactuables. Todos los elementos heredan de la clase **ItemManager** que les proporciona en forma de funciones los eventos importantes como el contacto con el personaje como el contacto con los enemigos.
Dentro de los de tipo loot o botín, tenemos la madera, las llaves del cofre y las llaves de la puerta.
- La madera es controlada por el script **WoodItemManager** y solamente se encarga de autodestruirse cuando colisiona con el jugador, además de sumarle madera al contador del jugador.
- Las llaves compartirán script pero con un enum que las permitirá diferenciar (**KeysType**). El script se llama **KeysItemManager**. En función del tipo de llave cambiará una propiedad del personaje u otra. Por temas de escalabilidad y de cumplimiento de los principios SOLID sería recomendable separarlo en dos scripts, pero finalmente se abordó la opción del enum.

Como elementos colocados por el jugador tendríamos solamente las fogatas.
- En las fogatas también tenemos dos tipos, en este caso sí que tendrá diferentes scripts: **BonfireItemManager** para la fogata normal (que permite matar a todos los enemigos excepto Slenderman) y **MagicBonfireItemManager** para destruir a absolutamente a todos los enemigos (menos cuando Slenderman está enfadado). Aparte de esta diferencia tenemos también la cuestión de que las fogatas normales se acaban extinguiendo, mientras que la mágica es eterna y además solo se podrá poner una.
Para 

En los elementos interactuables dispondríamos de la puerta de salida, el cofre y la luz general.
- La puerta de salida es controlada por el script **EndDoorManager** y únicamente valida que el personaje tenga las llaves adecuadas para permitirle interactuar con ella y lanzar la animación correspondiente. De lo contrario mostrará un mensaje conforme no tiene las llaves. 
- El cofre es controlado por el script **ChestItemManager** y al igual que la puerta, valida que el personaje tengas las llaves correctas para ser abierto. Mostrando también un mensaje en caso de no tenerlas. También dispone de una animación junto con la apertura de un panel revelando el contenido de la misma al jugador.
- La luz general cuenta con el script **GeneralLightManager** que no tiene dispone de ninguna condición para ser usado, únicamente se encarga de cambiar la animación y de notificar al **GameManager** el cambio de estado de la luz.
Los tres scripts heredan de una misma interface **Interactable** que permite que el script personaje no necesite saber exactamente con qué clase interactua y así ser almacenado para cuando el jugador decida interactuar con ellos. El hecho de que se use una interaz para esto agrega escalabilidad en caso de añadir más elementos futuros.

Como último script tendríamos el llamado **GameInputs** que controla todas las acciones externas al protagonista que el jugador desea realizar, en este caso las siguientes:
- Colocar fogatas
- Abrir menú pausa
- Apagar/encender linterna
- Interactuar con los elementos

### Capa visual
Dentro de esta capa tenemos tanto los scripts que controlan los menús (no importantes realmente y que serían **CreditsMenu**, **MainMenu**, **TitleMenu**) como los encargados del videojuego que sí comentaremos a continuación y finalmente algunas utilidades que se han realizado para el editor.
En el juego hemos usado los siguientes:
- **PlayerHUD**: Controla los elementos visuales propios del personaje (lo que llevaría en su mochila en ese momento), como el mostrar el número de troncos encontrados, las llaves (sean del cofre o de la puerta) y si se encuentra en posesión de la poción mágica o no.
- **InstructionsMenu**: Es el panel que saldrá la primera vez que juguemos (sin salir del videojuego) y que nos enseñaría la historia (con el propósito del jugador), los distintos elementos presentes del videojuego y los controles a utilizar.
- **ToggleLight**: Es el script que permitirá cambiar el sprite de las luces que tendremos en la esquina inferior derecha y que permite añade un extra de feedback al usuario para saber en todo momento el estado de las luces (la general y la de su linterna).
- **KeysSpriteManager**: Permitirá mostrar un tipo de llave u otra (siendo la imagen distinta) cuando el personaje las haya conseguido.
- **GameCanvasManager**: Sería el "controlador" de todo lo que tiene que ver con la parte visual, controla tanto los scripts anteriores como el resto de paneles o mensajes que se mostrarán durante la partida.
- **MessageManager**: Se encarga de manejar los dos tipos de mensajes que tenemos en el videojuego: el que se muestra en la parte superior como el que se muestra en la parte inferior. Ambos mensajes tendrán un comportamiento distinto que será dado gracias a este script.

El otro bloque visual y que en este caso serían utilidades, tenemos el siguiente par de scripts:
- **SortingLayerUtils**: Es una clase que expone únicamente un método llamado SortingLayerNames que serán todas los nombres de las capas dentro de los sorting layer, se retornará como un array de strings.
- **StringInListDrawer**: Es un atributo personalizado como el conocido SerializedField que permitirá transformarte la lista de strings que desees en un selector que podremos en el editor. En nuestro caso se usa para invocar al script **SortingLayerUtils** y así obtener todas las sorting layer válidas.


## Físicas y colisiones
### Componentes de física y colisiones del jugador
![Animaciones del protagonista](./Docs/ColliderAndPhysicalComponents.PNG)

En la imagen podemos ver los componentes Rigidbody2D y CapsuleCollider2D utilizados y con sus parámetros correspondientes.
De lo destacable respecto el videojuego de plataformas anterior es que en este tipo de videojuegos no usamos gravedad, ya que usamos el eje Y como si fuera el eje Z. Por lo demás es la misma configuración que un videojuego de plataformas.

### Tipos de colisiones llevadas a cabo en la partida

Tanto los enemigos como el protagonista tienen colisión con los elementos del mapa, ambos colisionan con los los bloques (tilemaps) y que les obliga a no salir de la zona del nivel pero además encontramos las colisiones con los ítems.
Para el jugador, se contemplan las siguientes colisiones:
	- Troncos de madera: ya que de esta forma le permite ir consiguiéndolos.
	- Llaves: para poder abrir el cofre y la puerta.
	- Cofre de la poción mágica: para poder interactuar con él.
	- Puerta de salida: para poder abrirla.
	- Luz general: para poder apagarla o encenderla.
	- Enemigos: cuando el protagonista es abatido por los enemigos.
	
En cambio, para los enemigos, tenemos las siguientes colisiones a contemplar:
	- Fogatas: ya que están son las armas que el protagonista tiene para destruirlos.
	- Linterna: al colisionar con estos rayos es cuando despiertan y comienzan a perseguir al protagonista
	- Protagonista: lo comentado anteriormente, para poder abatirlo si lo alcanzan.
	

## Aspectos visuales y sensoriales
Dentro de los aspectos visuales, explicaremos brevemente las diferentes animaciones que se han incluido en el videojuego:

**Animaciones del protagonista Dexter**

![Animaciones del protagonista](./Docs/PlayerController.PNG)
A diferencia de la práctica anterior, aquí solo estamos dos estados, _Idle_ y _Walk_. Aunque cada estado se trata realmente de un blend tree, y esto se ha hecho de esta manera para facilitar el movimiento en las dos direcciones (horizontal y vertical). De las cuales encontramos para cada estado cuatro animaciones distintas.

Para el blend tree de _Idle_, tenemos el siguiente blend tree:
![Blendtree Idle](./Docs/PlayerIdleBlendtree.PNG) 
En función de los parámetros de entrada Horizontal y Vertical (que son los que leemos en el script **PlayerMovement**), determinamos un sentido u otro en una de las dos direcciones posibles.

En _Walk_, tenemos un blend tree exactamente igual, solo que apuntando a otras animaciones:
![Blendtree Idle](./Docs/PlayerWalkBlendtree.PNG) 

Realmente, el control de estas animaciones es relativamente más sencillo, para cambiar de _Idle_ a _Walk_ usamos el parámetro booleano "IsWalking" y luego los parámetros "Horizontal" y "Vertical" determinan que animación activar siguiendo los mapas cartesianos anteriores.

**Animaciones de los enemigos**

Para el caso del enemigo, tenemos exactamente el mismo funcionamiento, solo que para extraer los parámetros Horizontal y Vertical usamos las componentes de velocidad normalizadas del navmesh agent.

**Animaciones de Slenderman**

![Animaciones de Slenderman](./Docs/SlendermanController.PNG) 

El enemigo que tiene ligeramente un cambio es Slenderman, ya que al tener dos modos (furioso y calmado), sus sprites también cambian. Pero ya que en la práctica anterior tuvimos un caso similar para Mario Bros con el tema de los powerups, aquí pudimos aplicar lo mismo. Así que creamos una nueva layer para el estado furioso con la opción sync para usar el mismo diagrama que la layer por defecto.
Luego, los blend tree sí que tienen que crearse de nuevo pero con la misma estructura que los anteriores vistos pero apuntando a las animaciones correspondientes.
Con esto, Slenderman comprende hasta un total de 16 animaciones (4 por _Idle_ más 4 por _Walk_ multiplicado por los 2 estados posibles).

**Animaciones del cofre**

![Animaciones del cofre](./Docs/ChestController.PNG) 

Solo hay los estados _Closed_ y _Open_ y de los cuales se transiciona mediante el parámetro "IsOpen". Una vez hecho esto, el cofre se queda en la última animación.

**Animaciones de la puerta**

Exactamente igual que el cofre.

**Animaciones de la luz general**

![Animaciones de la luz general](./Docs/GeneralLightController.PNG)

El diagrama es también sencillo excepto que en este caso es bidireccional, ya que una vez apagada la luz puede volver a encenderse. Esto es controlado por el parámetro "Enabled".

**Animaciones de la fogata normal**

![Animaciones de la fogata normal](./Docs/BonfireController.PNG)

Disponemos de 3 animaciones distintas: _Fired_, _MediumFired_ y _Extinct_.
Tal como el nombre explica, son las 3 transiciones hasta que finalmente el fuego se extingue. De esta forma evitamos crear una aglomeración de fogatas en el nivel. Los parámetros de control son "MediumFire" y "Extinct".

**Animaciones de la fogata mágica**

![Animaciones de la fogata mágica](./Docs/MagicBonfireController.PNG)

Dado que esta fogata es eterna, existe un único estado y es estar siempre encendida.

### Generación de la malla luminosa del jugador (URP)
A comienzos de la práctica empecé con un spot light como linterna con un material difuso para los sprites y que podía colocar donde quisiese con el ratón. El problema era que carecía de realismo y por ello hice una extensa búsqueda de luces 2D en Unity, ví que existían las light 2D experimental pero finalmente me decanté por un tutorial de Code Monkeys donde enseñaba a crear mesh renderer con distintos rayos y así dar un efecto de linterna. Me pareció super interesante ya que además de facilitarme las colisiones con los enemigos (cosa que no veía trivial con spot light), aprendí bastante sobre el proceso.
Para ello, se hizo uso de la pipeline URP de Unity, antiguamente se llamaba Lightweight Pipeline pero con el tiempo acabó cambiando de nombre. Lo que se configuró en esta pipeline es que la malla de la linterna tuviera un material de tipo alpha para convertirse en un material transparente, pero además se crearon 3 diferentes layer mask:
	- Mask: sería la capa usada tanto para la luz de la linterna como para la esfera que rodea al jugador.
	- BehindMask: es la capa interior que se renderizará cuando se haya renderizado primero la capa Mask. En pocas palabras, renderiza todo lo que Mask esté ocupando.
	- Black: es lo opuesto a BehindMask, renderiza todo lo que no esté siendo ocupado por la capa Mask. En las zonas donde Mask no sea visible.

Para entenderlo mejor, se tiene que ver como una combinación de filtros que dan a lugar distintos resultados en función de como se solapen estas capas.


Dentro de los aspectos sensoriales, destacamos el uso del script **AudioManager** que permite tener centralizada la parte de los efectos de sonido y el sonido de fondo. En este caso, además, se ha usado para reproducir el sonido del menú principal (usándolo como un objeto DontDestroyOnLoad).

## Malla de navegación 2D (navmesh 2D)

Se sabe de la existencia del componente Navmesh para 3D que viene incorporado con Unity de serie. Pero a la hora de investigar acerca de la creación de mallas para 2D encontré que la solución era usar un asset de terceros. Este asset es descargable via Github y pertenece al usuario h8man.
Con este asset, tenemos un funcionamiento muy parecido al 3D. A cada agente le asignamos el componente navmesh agent, a las zonas caminables y no caminables les asignamos el Navmesh modifier (especificando si es caminable o no) y finalmente creamos un objeto vacío con el script Navmesh surface 2D desde el cual generaremos la malla de navegación.
Para generar la malla correctamente en un juego top-down, simplemente le damos clic al botón "Rotate Surface to XY" o bien simplemente ponemos -90 en la componente X del vector de rotación.
Una vez hecho esto, podemos darle a "Bake" y finalmente nos quedaría algo como lo siguiente:

![Malla de navegación de nuestro nivel](./Docs/NavmeshSurface2D.PNG)

Podemos apreciar que todos las murallas quedan fuera de la zona azul, así como algunos objetos interactuables como el cofre o la luz general. Para lograr esto el suelo se ha divido en suelo caminable y no caminable. Aunque podría haberse conseguido seguramente usando la componente de obstáculo para Navmesh.

## Guía para completar la historia

Como comentamos al principio, dejaremos aquí la guía para completar la historia.
1. Primero empezamos delante de la puerta de salida, hay que tener en cuenta que esta localización es donde volveremos para escapar.
2. Coger los troncos de madera es necesario para matar a los monstruos que no son Slenderman, alguno acabará soltando la llave del cofre.
3. Con la llave del cofre (la marrón que aparecerá en el HUD del jugador), deberemos buscar el cofre e interactuar con él. Al abrirlo obtendremos una poción mágica de color azul.
4. El siguiente paso será buscar, mediante nuestra linterna, a Slenderman. Pero hay que vigilar los sonidos espaciales ya que depende del estado en que se encuentre emitirá unos sonidos u otros. 
5. Si encontramos a Slenderman y lo alumbramos estando él calmado, podremos destruirlo si plantamos la fogata mágica (que requerirá de un tronco de madera + la poción anterior). Si todo va bien, lo mataremos y soltará la llave dorada.
6. Con esta llave, podremos volver a la puerta y escaparemos finalmente del sitio.

Esto sería la situación ideal, pero si al buscar a Slenderman lo pillamos enfadado, este se reirá de nosotros y aunque apaguemos la linterna para que deje de seguirnos, continuará siguiéndonos. Además, encenderá las luces alertando así a todo el resto de monstruos. Por lo que será una situación difícil de la que escapar y lo primero que haríamos es ir a apagar las luces de nuevo con la palanca que estará por el nivel también. Una vez apagadas y Slenderman vuelva a su estado calmado, podremos volver a intentarlo.

## Dificultades encontradas
No hemos tenido alguna dificultad bloqueante pero si comentaremos los aspectos más difíciles que hemos encontrado.

### Efecto de linterna para nuestro personaje
Antes mencionamos que finalmente optamos por la solución de usar URP para crear 3 tipos de máscaras y así crear el efecto de que al iluminar al monstruo éste aparece y que al dejar de iluminarlo desaparece.
Pero primero se intento con luces sencillas como un spotlight o un point light, aunque el problema era la detección de las colisiones de esta luz con el enemigo. 
A la hora de usar la solución del tutorial que vimos también tuvimos dificultades, ya que este tutorial se encontraba en lightweight, mientras que ahora esa pipeline ya no existe y sería lo que ahora se denomina URP.
A pesar de ello, el resultado final es bastante agradable y además tenemos esa forma de flecha o arco que realiza la linterna que le da un enfoque muy atractivo.

### Seguimiento de los enemigos y colisiones con el protagonista
Una vez añadidas las componentes de agente para Navmesh, empezó un gran periodo de pruebas para acabar de ajustar lo mejor posible las velocidades y las colisiones con el jugador. El mayor problema es que los enemigos quedarán sincronizados respecto sus animaciones pero había veces que al acercarse a nosotros y frenar se mantenían en el estado _Walk_ de la animación. Para solucionar esto se tuvo que añadir una función que detectase si ya habíamos llegado a nuestro objetivo (personaje) y así quitar estos pequeños detalles que dañan la calidad del videojuego.

### Encontrar los sprites y los sonidos adecuados
Aunque pueda parecer una tontería, gran parte del tiempo se pierde buscando los sprites y sonidos que queremos para nuestro videojuego. Incluso aunque estemos dispuestos a pagar, sigue habiendo poco material. Por lo que fue una gran odisea el encontrar los sprites y sonidos que mejor encajasen en este tipo de videojuego.
De hecho, como primera opción se quería hacer un videojuego de disparos, pero para ello se requerían sprites más especifícos y que a la vez fuera compatible estéticamente con el resto de sprites.

### Convivencia de los distintos sonidos
Encontrar la armonía entre los distintos sonidos conviviendo juntos es algo complicado, ya que a veces pueden solaparse sonidos y eso puede provocar grandes molestias al jugador. Se ha intentado que fuera un audio de suspense pero sin que molestara al jugador. Creo que no se ha llegado a conseguir el resultado deseado pero al menos se ha intentado balancear lo máximo posible.

# Presentación del videojuego
- [Demostración del videojuego](https://youtu.be/WQ3nqMTm_Qw)

# Publicación
- WebGL: [Un juego de misterio para WebGL](https://informaticoloco-33.itch.io/unjuegodemisterio)

# Licencias / Recursos utilizados
- Sonidos: [Freesound](https://freesound.org/) y [Zapsplat](https://www.zapsplat.com/) (bajo licencia de atribución).
- Fuente de letra: [Google font](https://fonts.google.com/specimen/Nosifer) (uso libre).
- Imágenes: [Pinterest](https://www.pinterest.es/), [Asset store](https://assetstore.unity.com/packages/2d/environments/medieval-pixel-art-asset-free-130131) y [Rpg Maker](https://www.rpgmakerweb.com/) (no son todos aptos para uso comercial).
- Assets: [Navmesh 2D](https://github.com/h8man/NavMeshPlus) (uso libre)